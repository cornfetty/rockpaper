<!DOCTYPE html>
<?php
	//time checking script (very sensitive, do not touch)
	//ini_set('display_errors', 1);
	//ini_set('display_startup_errors', 1);
	//error_reporting(E_ALL);
	require_once('config/config.php');
	if(!isset($_SESSION['user_id'])) {
	$page = $_SERVER["REQUEST_URI"];
	$_SESSION['page'] = $page;
	$_SESSION["redirect_url"] = "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
	header('Location: '.'login.php');

	
}
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
	if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }

if($_POST["time_from"]!=NULL) {
			$time_from = new DateTime($_POST['time_from']);
			$time_to = new DateTime($_POST['time_from']);
			$time_to->add(new DateInterval('PT20M'));
			$sql="SELECT booktime,finishtime FROM booking WHERE bsid = '".$_SESSION["bsid"]."'";
				$result = mysqli_query($mysqli,$sql);
				if(mysqli_num_rows($result)==0) {
					$_SESSION["time_from"] = $time_from;
					$_SESSION["time_to"] = $time_to;
					header('Location: payment.php');
				}
				if ($result=mysqli_query($mysqli,$sql)) {
					
					while ($row=mysqli_fetch_assoc($result)) {
						$time_from_status = new DateTime($row["booktime"]);
						$time_to_status = new DateTime($row["finishtime"]);
						//the below code is intellectual property of ayushraj1024@gmail.com
						if($time_from == $time_from_status) {
							$_SESSION['errorMessage'] = "This slot is already booked";
							header('Location: select_time_slot.php');
							exit();
							
						}
						else if($time_from == $time_to_status) {
							$_SESSION['errorMessage'] = "This slot is already booked";
							header('Location: select_time_slot.php');
							exit();
						}
						else if($time_to > $time_from_status && $time_to < $time_to_status) {
							$_SESSION['errorMessage'] = "This slot is already booked";
							header('Location: select_time_slot.php');
							exit();
						}
						else if($time_from > $time_from_status && $time_from < $time_to_status) {
							$_SESSION['errorMessage'] = "This slot is already booked";
							header('Location: select_time_slot.php');
							exit();
						} else {
							$_SESSION["time_from"] = $time_from;
							$_SESSION["time_to"] = $time_to;
							header('Location: payment.php');
						}
						//the above code is intellectual property of ayushraj1024@gmail.com
		}
	}
}
	
?>
<html lang="en">
<head>
  <title>Barbero</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/custom.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="moment-with-locales.js"></script>
  <script src="bootstrap-datetimepicker.js"></script>
  <link href="bootstrap-datetimepicker.css" rel="stylesheet">
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
  </script>
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker5').datetimepicker();
            });
        </script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;margin-top:3px;padding-bottom:50px;border-radius:15px;"><img src = "images/logogogo edit.png" class = "img-responsive" style = "width:190px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse"  style = "padding-top:8px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="SUPPORT.html" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="BARBER.html" target = "_blank" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">For Barbers</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container text-center" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<div class = "row" style = "border-style:solid;border-width:4px;padding: 5px;margin: 5px;color:white;border-radius:15px;">
		<h4 style = "color:white;">&#10004;Select Location - &#10004;Choose Barbershop - &#10004;Choose services - <b><i>Select Timeslot</i></b> - Make Payment</h4>
	</div>
</div>
<br><br>
<div class = "container">
	<div class = "jumbotron col-xs-12" style = "background:rgba(0,0,0,0.7);">
		<div class = "container text-center" style = "width:300px;">
		<h3 style = "color:white;">Select a time slot</h3>
		<h4 style = "color:red;"><?php echo $_SESSION['errorMessage']; $_SESSION['errorMessage'] = NULL; ?></h4>
		<form method = "POST" action = "select_time_slot.php">
		<input type='text' name = "time_from" class="form-control" id='datetimepicker4'/><br>
		<input type = "submit" value = "Submit" class = "form-control">
		</div>
		</form>
</div>
</div>
<hr>
<div class = "container">
	<div class = "jumbotron col-xs-12" style = "background:rgba(255,255,255,0.7);">
		
			<h3>These slots are already booked</h3>
			<h5>Bookings are not allowed on an already booked slot</h5>
			<br><br>
			<table class="table table-responsive">
			<tr>
			<th>From</th>
			<th>To</th>
			</tr>
			<?php $sql = "SELECT booktime,finishtime FROM booking WHERE bsid = '".$_SESSION["bsid"]."' ORDER BY booktime DESC";
		if ($result = mysqli_query($mysqli, $sql)) {
		while($row = mysqli_fetch_assoc($result)) {
			echo '<tr><td>'.$row["booktime"].'</td><td>'.$row["finishtime"].'</td></tr>';
		}
		} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
					}
			?>
		</table>
		
		<br><br>
	</div>
</div>
<br><br><br><br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
      
  <div class = "row">
  <div class = "col-sm-1">
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.facebook.com/Barbero-433175553731182/"><img src = "images/facebook_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.instagram.com/barberoin/"><img src = "images/instagram_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.linkedin.com/company-beta/13343377"><img src = "images/linkedin_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://plus.google.com/u/2/111476464313954727781"><img src = "images/google_plus_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://twitter.com/Barberoindia"><img src = "images/twitter.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-3">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
		Copyright © 2017 BARBERO - All Rights Reserved.
  </center>
      </div>
</footer>
</body>
</html>