<!DOCTYPE html>
<?php
require_once('config/config.php');
if(!isset($_SESSION['user_id'])) {
	$page = $_SERVER["REQUEST_URI"];
	$_SESSION['page'] = $page;
	$_SESSION["redirect_url"] = "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
	header('Location: '.'login.php');
}
if($_GET["bofsid"]==NULL) {
	header('Location: '.'login.php');
}
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
$sql="SELECT * FROM barberofshop WHERE bofsid = '".$_GET["bofsid"]."'"; /*specifying the query to run*/
$result=mysqli_query($mysqli,$sql); /*loading the query in $result variable*/
while($row = mysqli_fetch_assoc($result)) {
$_SESSION["barbername"] = $row["name"];
}
$_SESSION["bofsid"] = $_GET["bofsid"];
$_SESSION["barberrating"] = $_GET["barberrating"];
?>
<html lang="en">
<head>
  <title>Barbero</title>
      <link rel="SHORTCUT ICON" href="images/sssssshhh.png">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
        #uberbar {
		position: fixed;
		background: white;
		width: 100%;
		z-index: 99;
}
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
  <script type = "text/javascript">
  var services = [];
  var total_price = 0;
  var total_time = 0;
  var k=0;
  var j=0;
  var flag = 0;
  function disable_submit() {
	  document.getElementById("mysubmit").disabled = true;
  }
  
function add_service(type,price,duration) {
	  for(j=0;j<=k;j++) {
		  if(services[j]!=type) {
			  flag = 1;
		  } else {
			  flag = 0;
			  break;
		  }
	  }
	  if(flag==1) {
	  var info_element_1 = document.createElement("p");
	  var info_1 = document.createTextNode(type);
	  var info_element_2 = document.createElement("p");
	  var info_2 = document.createTextNode("Rs. "+price+" | Duration "+duration+" minutes");
	  var hr = document.createElement("hr");
	  var remove_link = document.createElement("a");
	  var info_3 = document.createTextNode("(x)remove")
	  remove_link.setAttribute("href","#");
	  remove_link.setAttribute("onclick","remove_service('"+type+"',"+price+","+duration+")");
	  var element = document.getElementById("selected_services");
	  var service_div = document.createElement("div");
	  service_div.setAttribute("id",type);
	  remove_link.appendChild(info_3);
	  info_element_1.appendChild(info_1);
	  info_element_2.appendChild(info_2);
	  service_div.appendChild(info_element_1);
	  service_div.appendChild(info_element_2);
	  service_div.appendChild(remove_link);
	  service_div.appendChild(hr);
	  element.appendChild(service_div);
	  services[k] = type;
	  k++;
	  total_price+=price;
	  total_time+=duration;
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var service_post = document.getElementById(type+"_post");
	  service_post.setAttribute("value",1);
	  document.getElementById("mysubmit").disabled = false;
	  flag = 0;
		}
  }
function remove_service(type,price,duration) {
	  var x = document.getElementById("selected_services");
	  var y = document.getElementById(type);
	  x.removeChild(y);
	  total_price-=price;
	  total_time-=duration;
	  for(j=0;j<=k;j++) {
		  if(services[j]==type) {
			  services[j] = "";
			  k--;
			  break;
		  }
	  }
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var service_post = document.getElementById(type+"_post");
	  service_post.setAttribute("value",0);
	  if(k<=0) {
			  k = 0;
			  document.getElementById("mysubmit").disabled = true;
		  
	  } 
	  
  }
  </script>
</head>
<body onunload = "disable_submit()">
    <div id="uberber">
<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:black;margin-top:3px;padding-bottom:50px;border-radius:15px;"><img src = "images/logogogo edit.png" class = "img-responsive" style = "width:190px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse"  style = "padding-top:8px;" id="myNavbar">
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:black;background:rgba(0,0,0,0.6);border-style:;border-width:3px;">Home</a></li>
        <li><a href="SUPPORT.html" style="color:black;background:rgba(0,0,0,0.6);border-style:;border-width:3px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:black;background:rgba(0,0,0,0.6);border-style:;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:black;background:rgba(0,0,0,0.6);border-style:;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:black;background:rgba(0,0,0,0.6);border-style:;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:black;background:rgba(0,0,0,0.6);border-style:;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="BARBER.html" target = "_blank" style="color:black;background:rgba(0,0,0,0.6);border-style:;border-width:3px;">For Barbers</a></li>
      </ul>
    </div>
  </div>
            </nav></div><br><br><br><br><br><br>
<div class = "container text-center" style = "background:white;border-radius:2px;box-shadow:2px 2px 5px 10px #e6e6e6;"><br>
      <h4 style = "color:black;">Location ------ Barbershop ------ <b><i>Barbers</b></i> ------ services ------  Timeslot ------ Payment</h4>
	<br></div>

<br><br>
<form method = "post" action = "select_time.php">
<div class = "container">
<div class = "col-sm-8" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<h3 style = "color:white;">Choose services you want</h3>
	<hr>
	    <br><br>
			<?php
			$sql="SELECT * FROM service WHERE bsid = '".$_SESSION["bsid"]."'"; /*specifying the query to run*/
			$result=mysqli_query($mysqli,$sql); /*loading the query in $result variable*/
			while($row = mysqli_fetch_assoc($result)) {
			$type = $row["type"];
			$price = $row["price"];
			$duration = $row["duration"];
			echo '<div class = "row">
				<div class = "col-sm-4">
					<img src = "images/haircut.jpg" style = "height:auto;width:200px;border-radius:15px;" class = "img-responsive" ></img>
				</div>
				<div class = "col-sm-8" style = "padding-left:5%;">
					<h2 style = "color:white;">'.$type.'</h2>
					<h3 style = "color:white;">Rs. '.$price.' | Duration '.$duration.' minutes</h3>
					<input type = "button" onclick="add_service(\''.$type.'\','.$price.','.$duration.')" value = "Select" class = "btn btn-default">
				</div>
			</div>
	    <hr>';
			}
		?>
			
<br><br>
</div>

<div class = "col-sm-4" style = "color:white;background:rgba(0,0,0,0.9);border-radius:15px;">
			<h3>
			<div style = "float:left;">
			Total Amount (Rs.)
			</div>
			<div style = "float:right;" id = "total_price">
			0.00
			</div>
			</h3>
			<br><br>
			<h4>
			<div style = "float:left;">
			Total Duration (minutes)
			</div>
			<div style = "float:right" id = "total_time">
			0
			</div>
			</h4>
			<br>
			<hr>
			<div id = "selected_services">
			</div>
			<?php
			$sql="SELECT * FROM service WHERE bsid = '".$_SESSION["bsid"]."'"; /*specifying the query to run*/
			$result=mysqli_query($mysqli,$sql); /*loading the query in $result variable*/
			while($row = mysqli_fetch_assoc($result)) {
			$type = $row["type"];
			echo '<input type = "hidden" id = "'.$type.'_post" name = "'.$type.'">';
				}
			?>
			<input type = "hidden" id = "total_time_post" name = "total_time_post">
			<input type = "hidden" id = "total_price_post" name = "total_price_post">
			<input type = "submit" id = "mysubmit" name = "mysubmit" class = "form-control" value = "Proceed" style = "width:200px;float:right;" disabled>
			<br><br><br><br><br><br>
	
</div>
</div>
</form>
<br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
      
  <div class = "row">
  <div class = "col-sm-1">
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.facebook.com/Barbero-433175553731182/"><img src = "images/facebook_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.instagram.com/barberoin/"><img src = "images/instagram_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.linkedin.com/company-beta/13343377"><img src = "images/linkedin_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://plus.google.com/u/2/111476464313954727781"><img src = "images/google_plus_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://twitter.com/Barberoindia"><img src = "images/twitter.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-3">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
		Copyright © 2017 BARBERO - All Rights Reserved.
  </center>
      </div>
</footer>
</body>
</html>