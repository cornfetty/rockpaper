<!DOCTYPE html>
<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
unset($_SESSION["login_error"]);
require_once('config/config.php');
if(isset($_SESSION['user_id'])) {
	header('Location: '.'index.php');
}
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
 if($_POST["email"]!=NULL) {
			$sql = "SELECT * from user WHERE email = '".$_POST["email"]."'";
			$result=mysqli_query($mysqli,$sql);
			if(mysqli_num_rows($result)>1) {
				$_POST["email"] = NULL;
				$_POST["password"] = NULL;
				$_SESSION["login_error"] = "Invalid login. Try again.";
			}
			else if(mysqli_num_rows($result)==1) {
				$row = $result->fetch_assoc();
				if($row["password"]==$_POST["password"] && $row["valid"]==1) {
							$_SESSION["user_id"] = $row["uid"];
							$_SESSION["firstname"] = $row["firstname"];
							$_SESSION["lastname"] = $row["lastname"];
							$_SESSION["email"] = $row["email"];
						    $_SESSION["mobile"] = $row["mobile"];
							header('Location: '.$_SESSION["redirect_url"]);
				} else {
					$_POST["email"] = NULL;
					$_POST["password"] = NULL;
					$_SESSION["login_error"] = "Invalid email or password or the account has not been validated.";
				}
				
			}
			else if(mysqli_num_rows($result)==0) {
				$_SESSION["login_error"] = "Invalid login. Try again.";
				$_POST["email"] = NULL;
				$_POST["password"] = NULL;
			}
}
?>
<html lang="en">
<head>
  <title>Barbero</title>
  <link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
        
            #bgcolor {
	background: #efefef;
}
#uberbar {
		position: fixed;
		background: white;
		width: 100%;
		z-index: 99;
}
  </style>
</head>
<body>
     <div id="bgcolor">
<div id="uberbar">

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
                <a class="navbar-brand" href="index.php" style="background:white;color:black;padding-bottom:80px;border-radius:15px;"><img src = "images/new new loggozzzzzcdcdscsd.png" class = "img-responsive" style = "width:300px;height:300;"/></a>

    </div>
    <div class="collapse navbar-collapse"  style = "padding-top:8px;" id="myNavbar">
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
                    <li><a href="BARBER.html" target = "_blank" style="color:black;background:;border-style:;border-width:3px;">For Barbers</a></li>

        <li><a href="SUPPORT.html" style="color:black;background:;border-style:;border-width:3px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:black;background:#efefef;border-style:;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:black;background:;border-style:;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:black;background:);border-style:;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:black;background:;border-style:;border-width:3px;">Log out</a></li>';
		}
		?>
      </ul>             <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
 <li class="active"><a href="index.php" style="color:black;background:;border-style:;border-width:3px;">Home</a></li>

    </div>
  </div>
      </nav></div><br><br><br><br><br><br>
<div class = "container">
	<div class = "row text-center" style = "color:black;">
		<div class = "col-sm-4">
		</div>
		<div class = "col-sm-4" style = "background:white;padding:20px 20px 20px 20px;border-radius:15px;">
		<?php
			if($_POST["email"]==NULL) {
		echo '<h3>LOG IN</h3><hr>';
		echo '<h4 style = "color:red;">'.$_SESSION["login_error"].'</h4>';
		unset($_SESSION["login_error"]);
		echo '<form action = "login.php" method = "post">
			<input type = "email" id = "email" name = "email" class = "form-control" placeholder = "Enter email" required><br>
			<input type = "password" id = "password" name = "password" class = "form-control" placeholder = "Enter password" required><br><br>
			<input type = "submit" id = "submit" name = "submit" value = "Log in" class = "form-control">
			</form><br>'; 
		echo '<h4 style = "color:black;"><a href = "signup.php" style = "text-decoration:none;color:black;">New to Barbero? Sign up</a></h4>';
		echo '<h4 style = "color:black;"><a href = "changepasswordweb.php" style = "text-decoration:none;color:black;">Forgot password?</a></h4>';
		}
		?>
		</div>
		<div class = "col-sm-4">
		</div>
	</div>
</div>
<br><br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
      
  <div class = "row">
  <div class = "col-sm-1">
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.facebook.com/Barbero-433175553731182/"><img src = "images/facebook_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.instagram.com/barberoin/"><img src = "images/instagram_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.linkedin.com/company-beta/13343377"><img src = "images/linkedin_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://plus.google.com/u/2/111476464313954727781"><img src = "images/google_plus_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://twitter.com/Barberoindia"><img src = "images/twitter.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-3">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
		Copyright © 2017 BARBERO - All Rights Reserved.
  </center>
      </div>
</footer>
   
                  </div>
</body>
</html>
				