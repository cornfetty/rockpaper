<!DOCTYPE html> <!--declaring type of document-->
<?php  /*opening a PHP block*/
require_once('config/config.php');
if(!isset($_SESSION['user_id'])) {
	$page = $_SERVER["REQUEST_URI"];
	$_SESSION['page'] = $page;
	$_SESSION["redirect_url"] = "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
	header('Location: '.'login.php');
}
 /*including the file where database credentals are stored*/
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
?>

<html lang="en">
<head>
  <title>Barbero</title>
      <link rel="SHORTCUT ICON" href="images/sssssshhh.png">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
      #bgcolor {
	background: #efefef;
}
#uberbar {
		position: fixed;
		background: white;
		width: 100%;
		z-index: 99;
}
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }
.no_padding {
    margin: 0;
    padding: 0;
}
.btn_1.medium a.btn_1.medium{
    padding: 10px 25px;
    font-size: 14px;
}
.btn_1, a.btn_1 {
    border: none;
    font-family: inherit;
    color: #fff;
    background:rgba(0,0,0,0.6);
    cursor: pointer;
    padding: 7px 20px;
    display: inline-block;
    outline: 0
    transition: all .3s;
    border-radius: 3px;
    text-transform: uppercase;
    font-weight: 700;
}
.add_bottom_60 {
    margin-bottom: 60px;
	position: relative;
    top: -59px;
}
.add_bottom_45 {
    margin-bottom: 45px;
}
.main_title {
    text-align: center;
    font-size: 16px;
    margin-bottom: 30px;
}
div {
    display: block;
}
.main_title span {
    display: block;
    width: 40px;
    height: 4px;
    background-color: #ededed;
    margin: auto;
    border-radius: 5px;
    margin-bottom: 15px;
}
.main_title h2 {
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: -1px;
    font-size: 30px;
    margin-bottom: 0;
    margin-top: 0;
    color:grey;
}
#strip_activities ul {
    list-style: none;
    padding: 0 0 20px 0;
    margin: 0;
}
#strip_activities ul li {
    position: relative;
    min-height: 115px;
    padding: 5px 0 10px 150px;
    margin-bottom: 20px;
}
#strip_activities ul li img {
    position: absolute;
    left: 0;
    top: 0;
	height: 134px;
    width: 131px;
}
a.link_normal {
    position: relative;
    font-weight: 500;
    display: block;
    color: #1e4261;
    margin-top: 10px;
}
  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>
      <div id="bgcolor">
<div id="uberbar">

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:;color:black;margin-top:3px;padding-bottom:50px;border-radius:15px;"><img src = "images/new new loggozzzzzcdcdscsd.png" class = "img-responsive" style = "width:190px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse"  style = "padding-top:8px;" id="myNavbar">
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:black;background:;border-style:;border-width:3px;">Home</a></li>
        <li><a href="SUPPORT.html" style="color:black;background:;border-style:;border-width:3px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:black;background:;border-style:;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:black;background:;border-style:;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:black;background:rgba);border-style:;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:black;background:;border-style:;border-width:3px;">Log out</a></li>';
		}
		?>
            <li>   </ul>   <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
<a href="BARBER.html" target = "_blank" style="color:black;background:;border-style:;border-width:3px;">For Barbers</a></li>
      </ul>
    </div>
  </div>
      </nav></div><br><br><br><br><br><br><br>
<div class = "container text-center" style = "background:white;border-radius:2px;box-shadow:2px 2px 5px 10px #e6e6e6;"><br>
	<h4 style = "color:black;"><b><i>Location</b></i> ------ Barbershop ------ Barbers ------ services ------  Timeslot ------ Payment</h4>
	<br></div>

<br><br>
	<div class = "container">
		<div class = "jumbotron" style = "background:white;" id = "jumbotron_div">
			<div class = "container text-center" style = "width:400px;">
				<h3 style = "color:black;" id = "text_1">Select Location</h3>
					<form action = "select_shop.php" method = "POST">
						<select class = "form-control" name = "city_id" id = "city_id">
							  <?php
							    $sql="SELECT city_id,city_name,city_state from cities ORDER BY city_name ASC"; /*specifying the query to run*/
								$result=mysqli_query($mysqli,$sql); /*loading the query in $result variable*/
								while($row=mysqli_fetch_assoc($result)) { /*starting a while loop to traverse through the results returned by our query*/
								echo '<option value="'.$row["city_id"].'">'.$row["city_name"].', '.$row["city_state"].'</option>';
								}
								?>
						</select><br>
						<input type = "submit" name = "submit" id = "submit" class = "form-control" value = "Search">
					</form>
			</div>
		</div>
	</div>
	
<br><br><br><br><br><br><br><br><br><br>
      <div class="container" style="width:800;position: relative; top: -142px;background:white;">
           <center> <h1>POPULAR BARBER SHOPS</h1></center>
			<hr style="color:black">
            <div class="row">
                <div class="col-sm-4">
                    <a href="#" ><div class="thumbnail">
                            <img src="images/Barbero_shop_pic.jpeg" width=400 height=400 alt="">
                            <div class="caption">
                                <h3><b>HAIR CUTS</h3></b>
                                <i><p>best services</p></i>
                            </div>
                        </div> 
                    </a>
                </div>

                <div class="col-sm-4">
                    <a  href="#"  ><div class="thumbnail">
                            <img src="images/Barbero_shop1.jpeg" width=400 height=600 alt="">
                            <div class="caption">
                                <h3><b>MR AND MRS</h3></b>
                                <i><p>Finest barbers</p></i>
                            </div>
                        </div> 
                    </a>
                </div>

                <div class="col-sm-4">
                    <a  href="#"  ><div class="thumbnail">
                            <img src="images/Barbero_shop_pics.jpg" width=400 height=400 alt="">
                            <div class="caption">
                                <h3><b>HANDSOME CORE</b></h3>
                                <i><p>wide range of hairstyles</p></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
		<br>
		
		</div><br><br>


<div class="container" style="width:800;position: relative; top: -142px;background:white;">
<div class="container add_bottom_60">

		<div class="main_title add_bottom_45">
			<span></span><br><br><br><br>
			<h2 style="color:black;">Hair Trends</h2>
			<p>Read about new hair styles</p>
			<hr style="color:black">
		</div>
		<div class="row">
			<div class="col-md-8" id="strip_activities">
                <ul>
                    <li>
                    <img src="images/1.jpg" alt="" class="img-circle styled">
                    <h4>Textured Pomp</h4>
                    <p>
                        This fresh take on the pomp fade maintains that classic shape but with plenty of texture and artful movement. 
                    </p>
					<br>
                    </li>
                    <li><img src="images/2.jpg" alt="" class="img-circle styled">
                    <h4>Wavy Slick Back </h4>
                    <p>
                        This trends works well with natural wave. Use a curl defining product for hold and perfect waves. 
                    </p>
                    </li>
					<br>
                    <li><img src="images/3.jpg" alt="" class="img-circle styled">
                    <h4>Flow + Line Up</h4>
                    <p>
                        Finger-styled hair contrasts with an angular line up and blurry fade.
                    </li>
					<br>
                    <li><img src="images/4.jpg" alt="" class="img-circle styled">
                    <h4>Tapered Haircut</h4>
                    <p>
                        Movement and flow can happen all over the head, continuing from the front to the back.
                    </p>
                    </li>
                </ul>
            </div>
		</div>
	</div></div>
	<br><br>
	<div class = "container text-center" style = "background:white; color:white;border-radius:15px;padding-bottom:40px;position: relative;top: -74px;">
		<h3 style="font-size:250%;color:black"><b>DOWNLOAD THE APP</b></h3>
		<hr>
		<div class = "row text-center">
			<div class = "col-sm-6 text-center" style = "padding-top:5%;">
			    <center>
				<h3 style="color:black">Download our android application</h3>
				<h3 style="color:black">Get it on google play</h3>
				<a href = "#"><img src = "images/google_play_icon.png" class = "img-responsive" style = "width:300px;height:auto;"/></a>
				</center>
			</div>
			<div class = "col-sm-6" style = "padding-top:5%;">
				<center>
				<img src = "images/phoneapp.png" class = "img-responsive" style = "width:120px;height:auto;"/>
				</center>
			</div>
		</div>
	</div>
	<br><br></div>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
      
  <div class = "row">
  <div class = "col-sm-1">
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.facebook.com/Barbero-433175553731182/"><img src = "images/facebook_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.instagram.com/barberoin/"><img src = "images/instagram_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.linkedin.com/company-beta/13343377"><img src = "images/linkedin_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://plus.google.com/u/2/111476464313954727781"><img src = "images/google_plus_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://twitter.com/Barberoindia"><img src = "images/twitter.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-3">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
		Copyright © 2017 BARBERO - All Rights Reserved.
  </center>
      </div>
</footer>
   
      </div></div>
</body>
</html>