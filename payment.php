<!DOCTYPE html>
<?php
require_once('config/config.php');
if(!isset($_SESSION['user_id'])) {
	$page = $_SERVER["REQUEST_URI"];
	$_SESSION['page'] = $page;
	$_SESSION["redirect_url"] = "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
	header('Location: '.'login.php');
}
?>
<html lang="en">
<head>
  <title>Barbero</title>
  <link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
   <script type="text/javascript">

   //Created / Generates the captcha function    
    function DrawCaptcha()
    {
        var a = Math.ceil(Math.random() * 10)+ '';
        var b = Math.ceil(Math.random() * 10)+ '';       
        var c = Math.ceil(Math.random() * 10)+ '';  
        var d = Math.ceil(Math.random() * 10)+ '';  
        var e = Math.ceil(Math.random() * 10)+ '';  
        var f = Math.ceil(Math.random() * 10)+ '';  
        var g = Math.ceil(Math.random() * 10)+ '';  
        var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
        document.getElementById("txtCaptcha").value = code
    }

    // Validate the Entered input aganist the generated security code function   
    function ValidCaptcha(){
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (str1 == str2) return true;        
        var errorInput = document.getElementById("txtInput");
		errorInput.setAttribute("value","Invalid Captcha. Try again.");
		return false;
        
    }

    // Remove the spaces from the entered and generated code
    function removeSpaces(string)
    {
        return string.split(' ').join('');
    }
    
 
    </script>
</head>
<body onload="DrawCaptcha();">

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;margin-top:3px;padding-bottom:50px;border-radius:15px;"><img src = "images/logogogo edit.png" class = "img-responsive" style = "width:190px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse"  style = "padding-top:8px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="SUPPORT.html" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="BARBER.html" target = "_blank" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">For Barbers</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container text-center" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<div class = "row" style = "border-style:solid;border-width:4px;padding: 5px;margin: 5px;color:white;border-radius:15px;">
		<h4 style = "color:white;">&#10004;Select Location - &#10004;Choose Barbershop - &#10004;Choose services - &#10004;Select Timeslot - <b><i>Make Payment</i></b></h4>
	</div>
</div>
<br><br>
<div class = "container">
<div class = "col-sm-6 col-lg-6 col-md-6">
<div class = "row" style = "background:rgba(0,0,0,0.7);border-radius:15px;padding:20px 20px 20px 20px;">
	<h3 style = "color:white;">Choose a payment option and make payment</h3><br>
	<h4 style = "color:white;">Make Payment at Shop:</h4>
	<form action = "generate_receipt.php" method = "POST" onsubmit="return ValidCaptcha();">
	<input type="text" id="txtCaptcha" class = "form-control" style="background-image:url('images/cap.jpg');text-align:center; border:none; font-weight:bold;width:200px; font-family:Modern">
    <input type="button" id="btnrefresh" value="Refresh" onclick="DrawCaptcha();" style = "width:200px;" class = "form-control"><br><br>
	<input type="text" id="txtInput" placeholder = "Enter Captcha" style = "width:200px;" class = "form-control"> 
	<input type = "submit" value = "Generate Receipt" class = "form-control">
	</form>
</div>
</div>
<div class = "col-sm-6 col-lg-6 col-md-6">
<div class = "row" style = "background:rgba(0,0,0,0.9);border-radius:15px;padding:20px 20px 20px 20px;color:white;">
	<h3>Your order details</h3><br>
	<?php
		echo '<h4>Shop: '.$_SESSION["shopname"].'</h4><h5>Address: '.$_SESSION["addressline1"].'</h5><h5>&nbsp;'.$_SESSION["addressline2"].'</h5><h5> Contact: '.$_SESSION["shopcontact"].'</h5><hr>
		<h4>Barber: '.$_SESSION["barbername"].'</h4><hr><h4>Services:';
		for($i=0;$i<$_SESSION["no_of_services"];$i++) {
			echo '<h5>'.$_SESSION["typearray"][$i].'</h5><h5>Rs. '.$_SESSION["pricearray"][$i].' | Duration '.$_SESSION["durationarray"][$i].' minutes</h5><hr>';			
		
		}
		echo '<h4><div style = "float:left;">Total price (Rs.)</div><div style = "float:right;">'.$_SESSION["total_price"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Duration (minutes)</div><div style = "float:right;">'.$_SESSION["total_time"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Start time</div><div style = "float:right;">'.$_SESSION["time_from"]->format('d M Y h:i:s a').'</div></h4><br>';
		echo '<h4><div style = "float:left;">End time</div><div style = "float:right;">'.$_SESSION["time_to"]->format('d M Y h:i:s a').'</div></h4><br>';
	?>	
</div>
</div>
</div>
<br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-4">
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.facebook.com/rockpaperindia"><img src = "images/facebook_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.instagram.com/rockpaperindia/"><img src = "images/instagram_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.linkedin.com/rockpaperindia"><img src = "images/linkedin_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://plus.google.com/rockpaperindia"><img src = "images/google_plus_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-4">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
	Copyright © 2017 Rock Paper - All Rights Reserved.
  </center>
  </div>
</footer>
</body>
</html>