<?php
require_once('config/config.php');
require_once 'classes/phpmailer/mail.php';
						$mail1 = new PHPMailer(); // create a new object
						$mail1->IsSMTP(); // enable SMTP
						$mail1->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
						$mail1->SMTPAuth = true; // authentication enabled
						 // secure transfer enabled REQUIRED for Gmail
						$mail1->Host = $SMTPhost;
						$mail1->Port = 465; // or 587
						$mail1->IsHTML(true);
						$mail1->Username = $SMTPusername;
						$mail1->Password = $SMTPpassword;
						$mail1->SetFrom($SMTPfrom);
						$mail1->SMTPSecure = 'ssl';
						$mail1->Subject = "Password Reset | Barbero";
						$mail1->Body = "<p>You have requested for a password change. Please follow this link to change your password -></p><p><a href = '".$url."/changepassword.php?email=".$_POST["email"]."&validation=".$validation."'>Validate Account</a></p>";
						$mail1->AddAddress($mail_to);
						$mail1->SmtpClose();
						$mail1->Send();
?>