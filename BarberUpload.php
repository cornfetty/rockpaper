<?php
$target_dir = "barberImages/";
$target_file_name = $target_dir .basename($_FILES["file"]["name"]);
$result = array();
 
// Check if image file is a actual image or fake image
if (isset($_FILES["file"])) 
{
 if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file_name)) 
 {
  $success = true;
  $message = "Successfully Uploaded";
 }
 else 
 {
  $success = false;
  $message = "Error while uploading";
 }
}
else 
{
 $success = false;
 $message = "Required Field Missing";
}
 
$result["success"] = $success;
$result["message"] = $message;
echo json_encode($result);
 
?>