<!DOCTYPE html>
<?php
require_once('config/config.php');
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
?>
<html lang="en">
<head>
  <title>Rock Paper - Explore Salons</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/logo.png" class = "img-responsive" style = "width:180px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">About</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Projects</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['username'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Register Barber Shop</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container">
	<div class = "row text-center" style = "color:white;">
		<div class = "col-sm-4">
		</div>
		<div class = "col-sm-4" style = "background:rgba(0,0,0,0.7);padding:20px 20px 20px 20px;border-radius:15px;">
		<?php
		$email = $_GET["email"];
		$validation = $_GET["validation"];
		$sql="SELECT * from user WHERE email = '".$email."' AND validation = '".$validation."'";
				$result=mysqli_query($mysqli,$sql);
				if(mysqli_num_rows($result)==1) {
					$sql = "UPDATE user SET valid = 1 WHERE email = '".$email."'";
					mysqli_query($mysqli,$sql);
					echo '<h3>Thank you for validating your account</h3><br>
					<h4>You may now use your account on Rock Paper</h4>
					<a href = "login.php" style = "color:white;">Click here to login</a>';
				} else {
					echo '<h4>The account with mentioned email does not exist. Kindly sign up</h4>';
				}
		?>
		</div>
		<div class = "col-sm-4">
		</div>
	</div>
</div>
<br><br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-4">
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.facebook.com/rockpaperindia"><img src = "images/facebook_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.instagram.com/rockpaperindia/"><img src = "images/instagram_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.linkedin.com/rockpaperindia"><img src = "images/linkedin_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://plus.google.com/rockpaperindia"><img src = "images/google_plus_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-4">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
	Copyright © 2017 Rock Paper - All Rights Reserved.
  </center>
  </div>
</footer>
</body>
</html>
				