<!DOCTYPE html>
<?php
require_once('config/config.php');
?>
<html lang="en">
<head>
  <title>Rock Paper - Explore Salons</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/logo.png" class = "img-responsive" style = "width:180px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">About</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Projects</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Register Barber Shop</a></li>
      </ul>
    </div>
  </div>
</nav>
<br>
	<div class = "jumbotron" style = "background:rgba(0,0,0,0.6);background-image:url(images/haircut.jpg);background-size:cover;box-shadow:0px -20px 40px 0px black inset;">
		<div class = "container">
		<h1 style = "font-size:380%;color:white;">Why Wait For Your Turn</h1>
		<h1 style = "font-size:380%;color:white;">At Barber Shop</h1>
        <h1 style = "font-size:380%;color:white;">BOOK THE BEST</h1>
		<h1 style = "font-size:380%;color:white;">BARBER IN YOUR CITY</h1>
		<h2 style = "color:white;">Book An Appointment At A Barber Shop Near You.</h2><br>
		<h3><a href = "booking.php" style="background:rgba(0,0,0,0.6);color:white;border-style:solid;border-width:4px;padding: 15px;margin: 15px;text-decoration:none;">Book Online</a></h3>
		</div>
	</div>
	<div class = "container text-center" style = "background:rgba(0,0,0,0.7); color:white;border-radius:15px;padding-bottom:40px;">
		<h3><b>IT IS SIMPLE</b></h3>
		<hr>
		<div class = "row">
			<div class= "col-sm-4">
				<center>
				<img src = "images/choose_barber_shop.jpg" style = "height:200px;width:auto;border-radius:15px;" class = "img-responsive"/>
				<h4>Choose the barbershop in your area</h4>
				</center>
			</div>
			<div class= "col-sm-4">
			    <center>
				<img src = "images/index_image_2.jpg" style = "height:200px;width:auto;border-radius:15px;" class = "img-responsive"/>
				<h4>Choose the services you want</h4>
				</center>
			</div>
			<div class= "col-sm-4">
				<center>
				<img src = "images/get_haircut.jpg" style = "height:200px;width:auto;border-radius:15px;" class = "img-responsive"/>
				<h4>Pick a date and your appointment with barber is done. Get your haircut without waiting!</h4>
				</center>
			</div>
		</div>
	</div><br><br>
	<div class = "container text-center" style = "background:rgba(0,0,0,0.7); color:white;border-radius:15px;padding-bottom:40px;">
		<h3><b>BOOK FROM ANYWHERE YOU ARE</b></h3>
		<hr>
		<div class = "row text-center">
			<div class = "col-sm-6 text-center" style = "padding-top:15%;">
				<h3>With our reponsive design you can use our website on any platform</h3>
			</div>
			<div class = "col-sm-6">
				<img src = "images/platform_icon.png" class = "img-responsive"/>
			</div>
		</div>
	</div><br><br>
	<div class = "container text-center" style = "background:rgba(0,0,0,0.7); color:white;border-radius:15px;padding-bottom:40px;">
		<h3><b>DOWNLOAD THE APP</b></h3>
		<hr>
		<div class = "row text-center">
			<div class = "col-sm-6 text-center" style = "padding-top:5%;">
			    <center>
				<h3>Download our android application</h3>
				<h3>Get it on google play</h3>
				<a href = "#"><img src = "images/google_play_icon.png" class = "img-responsive" style = "width:200px;height:auto;"/></a>
				</center>
			</div>
			<div class = "col-sm-6" style = "padding-top:5%;">
				<center>
				<img src = "images/android_phone_icon.png" class = "img-responsive" style = "width:120px;height:auto;"/>
				</center>
			</div>
		</div>
	</div><br><br>
	<div class = "container text-center" style = "background:rgba(0,0,0,0.7); color:white;border-radius:15px;padding-bottom:40px;">
		<h3><b>WHAT PEOPLE ARE SAYING</b></h3>
		<hr>
		<div class = "row text-center">
			<div class = "col-sm-4" style = "padding-bottom:20px;">
				<center>
				<img src = "images/people_pic_1.jpg" class = "img-responsive" style = "width:100px;height:100px;border-radius:50%;"/><br>
				<h4>Deshali</h4>
				I love this service
				</center>
			</div>
			<div class = "col-sm-4" style = "padding-bottom:20px;">
			    <center>
				<img src = "images/people_pic_2.jpg" class = "img-responsive" style = "width:100px;height:100px;border-radius:50%;"/><br>
				<h4>xyz</h4>
				nice
				</center>
			</div>
			<div class = "col-sm-4" style = "padding-bottom:20px;">
				<center>
				<img src = "images/people_pic_3.jpg" class = "img-responsive" style = "width:100px;height:100px;border-radius:50%;"/><br>
				<h4>abc</h4>
				excellent
				</center>
			</div>
		</div>
	</div><br><br>
<div class = "container text-center" style = "background:rgba(0,0,0,0.7); color:white;border-radius:15px;padding-bottom:40px;">
		<h3><b>CONTACT US</b></h3>
		<hr>
		<h3>Better yet, See us in person!</h3><br>
		<h4>We love our customers, so feel free to visit during normal business hours.</h4><br>
		<h3>Rock Paper</h3>
		<h4>Kota, Rajasthan</h4><br>
		<div class = "row text-center">
			<h3><a href = "#" style="color:white;border-style:solid;border-width:4px;padding: 15px;margin: 15px;text-decoration:none;">Drop us a line</a></h3>
		</div>
	</div>
<br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-4">
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.facebook.com/rockpaperindia"><img src = "images/facebook_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.instagram.com/rockpaperindia/"><img src = "images/instagram_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.linkedin.com/rockpaperindia"><img src = "images/linkedin_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://plus.google.com/rockpaperindia"><img src = "images/google_plus_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-4">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
	Copyright © 2017 Rock Paper - All Rights Reserved.
  </center>
  </div>
</footer>

</body>
</html>
