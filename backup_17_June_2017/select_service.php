<!DOCTYPE html>
<?php
require_once('config/config.php');
?>
<html lang="en">
<head>
  <title>Rock Paper - Explore Salons</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
  <script type = "text/javascript">
  var haircutting=0;
  var shaving = 0;
  var trimming = 0;
  var total_price = 0;
  var total_time = 0;
  function disable_submit() {
	  document.getElementById("mysubmit").disabled = true;
  }
  
  function add_haircutting() {
	  if(!haircutting) {
	  var info_element_1 = document.createElement("p");
	  var info_1 = document.createTextNode("Haircutting");
	  var info_element_2 = document.createElement("p");
	  var info_2 = document.createTextNode("Rs. 30 | Duration 10 minutes");
	  var hr = document.createElement("hr");
	  var remove_link = document.createElement("a");
	  var info_3 = document.createTextNode("(x)remove")
	  remove_link.setAttribute("href","#");
	  remove_link.setAttribute("onclick","remove_haircutting()");
	  var element = document.getElementById("selected_services");
	  var haircutting_div = document.createElement("div");
	  haircutting_div.setAttribute("id","haircutting_div");
	  remove_link.appendChild(info_3);
	  info_element_1.appendChild(info_1);
	  info_element_2.appendChild(info_2);
	  haircutting_div.appendChild(info_element_1);
	  haircutting_div.appendChild(info_element_2);
	  haircutting_div.appendChild(remove_link);
	  haircutting_div.appendChild(hr);
	  element.appendChild(haircutting_div);
	  haircutting = 1;
	  total_price+=30;
	  total_time+=10;
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var haircutting_post = document.getElementById("haircutting");
	  haircutting_post.setAttribute("value",1);
	  document.getElementById("mysubmit").disabled = false;
	  }
	  if(haircutting) {
		  
	  }
  }
   function add_shaving() {
	  if(!shaving) {
	  var info_element_1 = document.createElement("p");
	  var info_1 = document.createTextNode("Shaving");
	  var info_element_2 = document.createElement("p");
	  var info_2 = document.createTextNode("Rs. 60 | Duration 20 minutes");
	  var hr = document.createElement("hr");
	  var remove_link = document.createElement("a");
	  var info_3 = document.createTextNode("(x)remove")
	  remove_link.setAttribute("href","#");
	  remove_link.setAttribute("onclick","remove_shaving()");
	  var element = document.getElementById("selected_services");
	  var shaving_div = document.createElement("div");
	  shaving_div.setAttribute("id","shaving_div");
	  info_element_1.appendChild(info_1);
	  info_element_2.appendChild(info_2);
	  remove_link.appendChild(info_3);
	  shaving_div.appendChild(info_element_1);
	  shaving_div.appendChild(info_element_2);
	  shaving_div.appendChild(remove_link);
	  shaving_div.appendChild(hr);
	  element.appendChild(shaving_div);
	  shaving = 1;
	  total_price+=60;
	  total_time+=20;
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var shaving_post = document.getElementById("shaving");
	  shaving_post.setAttribute("value",1);
	  document.getElementById("mysubmit").disabled = false;
	  }
	  if(shaving) {
		  
	  }
	  
  }
   function add_trimming() {
	  if(!trimming) {
	  var info_element_1 = document.createElement("p");
	  var info_1 = document.createTextNode("Trimming");
	  var info_element_2 = document.createElement("p");
	  var info_2 = document.createTextNode("Rs. 20 | Duration 10 minutes");
	  var hr = document.createElement("hr");
	  var remove_link = document.createElement("a");
	  var info_3 = document.createTextNode("(x)remove")
	  remove_link.setAttribute("href","#");
	  remove_link.setAttribute("onclick","remove_trimming()");
	  var element = document.getElementById("selected_services");
	  var trimming_div = document.createElement("div");
	  trimming_div.setAttribute("id","trimming_div");
	  remove_link.appendChild(info_3);
	  info_element_1.appendChild(info_1);
	  info_element_2.appendChild(info_2);
	  trimming_div.appendChild(info_element_1);
	  trimming_div.appendChild(info_element_2);
	  trimming_div.appendChild(remove_link);
	  trimming_div.appendChild(hr);
	  element.appendChild(trimming_div);
	  trimming = 1;
	  total_price+=20;
	  total_time+=10;
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var trimming_post = document.getElementById("trimming");
	  trimming_post.setAttribute("value",1);
	  document.getElementById("mysubmit").disabled = false;
	  }
	  if(trimming) {
		  
	  }
  }
  function remove_haircutting() {
	  var x = document.getElementById("selected_services");
	  var y = document.getElementById("haircutting_div");
	  x.removeChild(y);
	  total_price-=30;
	  total_time-=10;
	  haircutting = 0;
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var haircutting_post = document.getElementById("haircutting");
	  haircutting_post.setAttribute("value",0);
	  if(!shaving && !trimming) {
			  document.getElementById("mysubmit").disabled = true;
		  
	  } 
	  
  }
  function remove_shaving() {
	  var x = document.getElementById("selected_services");
	  var y = document.getElementById("shaving_div");
	  x.removeChild(y);
	  total_price-=60;
	  total_time-=20;
	  shaving = 0;
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var shaving_post = document.getElementById("shaving");
	  shaving_post.setAttribute("value",0);
	   if(!haircutting && !trimming) {
		  
			  document.getElementById("mysubmit").disabled = true;
		  }
	  
  }
  function remove_trimming() {
	  var x = document.getElementById("selected_services");
	  var y = document.getElementById("trimming_div");
	  x.removeChild(y);
	  total_price-=20;
	  total_time-=10;
	  trimming = 0;
	  document.getElementById("total_price").innerHTML = total_price;
	  document.getElementById("total_time").innerHTML = total_time;
	  var price_post = document.getElementById("total_price_post");
	  price_post.setAttribute("value",total_price);
	  var time_post = document.getElementById("total_time_post");
	  time_post.setAttribute("value",total_time);
	  var trimming_post = document.getElementById("trimming");
	  trimming_post.setAttribute("value",0);
	   if(!haircutting && !shaving) {
		  
			  document.getElementById("mysubmit").disabled = true;
		  }
  }
  </script>
</head>
<body onunload = "disable_submit()">

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/logo.png" class = "img-responsive" style = "width:180px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">About</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Projects</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Register Barber Shop</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container text-center" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<div class = "row" style = "border-style:solid;border-width:4px;padding: 5px;margin: 5px;color:white;border-radius:15px;">
		<h4 style = "color:white;">&#10004;Select Location - &#10004;Choose Barbershop - <b><i>Choose services</i></b> - Select Timeslot - Make Payment</h4>
	</div>
</div>
<br><br>
<form method = "post" action = "select_time.php">
<div class = "container">
<div class = "col-sm-8" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<h3 style = "color:white;">Choose services you want</h3>
	<hr>
	    <br><br>
			<div class = "row">
				<div class = "col-sm-4">
					<img src = "images/haircut.jpg" style = "height:auto;width:200px;border-radius:15px;" class = "img-responsive" ></img>
				</div>
				<div class = "col-sm-8" style = "padding-left:5%;">
					<h2 style = "color:white;">Haircutting</h2>
					<h3 style = "color:white;">Rs. 30 | Duration 10 minutes</h3>
					<input type = "button" onclick="add_haircutting()" value = "Select" class = "btn btn-default">
				</div>
			</div>
	    <hr>
			<div class = "row">
				<div class = "col-sm-4">
					<img src = "images/shaving.jpg" style = "height:auto;width:200px;border-radius:15px;" class = "img-responsive"></img>
				</div>
				<div class = "col-sm-8" style = "padding-left:5%;">
					<h2 style = "color:white;">Shaving</h2>
					<h3 style = "color:white;">Rs. 60 | Duration 20 minutes</h3>
					<input type = "button" onclick="add_shaving()" value = "Select" class = "btn btn-default">
				</div>
			</div>
		
		<hr>
			<div class = "row">
				<div class = "col-sm-4">
					<img src = "images/trimming.jpg" style = "height:auto;width:200px;border-radius:15px;" class = "img-responsive"></img>
				</div>
				<div class = "col-xs-8" style = "padding-left:5%;">
					<h2 style = "color:white;">Trimming</h2>
					<h3 style = "color:white;">Rs. 20 | Duration 10 minutes</h3>
					<input type = "button" onclick="add_trimming()" value = "Select" class = "btn btn-default">
				</div>
			</div>
<br><br>
</div>

<div class = "col-sm-4" style = "color:white;background:rgba(0,0,0,0.9);border-radius:15px;">
			<h3>
			<div style = "float:left;">
			Total Amount (Rs.)
			</div>
			<div style = "float:right;" id = "total_price">
			0.00
			</div>
			</h3>
			<br><br>
			<h4>
			<div style = "float:left;">
			Total Duration (minutes)
			</div>
			<div style = "float:right" id = "total_time">
			0
			</div>
			</h4>
			<br>
			<hr>
			<div id = "selected_services">
			</div>
			<input type = "hidden" id = "haircutting" name = "haircutting">
			<input type = "hidden" id = "shaving" name = "shaving">
			<input type = "hidden" id = "trimming" name = "trimming">
			<input type = "hidden" id = "total_time_post" name = "total_time_post">
			<input type = "hidden" id = "total_price_post" name = "total_price_post">
			<input type = "submit" id = "mysubmit" name = "mysubmit" class = "form-control" value = "Proceed" style = "width:200px;float:right;" disabled>
			<br><br><br><br><br><br>
	
</div>
</div>
</form>
<br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-4">
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.facebook.com/rockpaperindia"><img src = "images/facebook_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.instagram.com/rockpaperindia/"><img src = "images/instagram_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.linkedin.com/rockpaperindia"><img src = "images/linkedin_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://plus.google.com/rockpaperindia"><img src = "images/google_plus_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-4">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
	Copyright © 2017 Rock Paper - All Rights Reserved.
  </center>
  </div>
</footer>
</body>
</html>