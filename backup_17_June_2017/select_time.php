<!DOCTYPE html>
<?php
	require_once('config/config.php');
	$_SESSION["haircutting"] = $_POST["haircutting"];
	$_SESSION["shaving"] = $_POST["shaving"];
	$_SESSION["trimming"] = $_POST["trimming"];
	$_SESSION["total_price"] = $_POST["total_price_post"];
	$_SESSION["total_time"] = $_POST["total_time_post"];
?>
<html lang="en">
<head>
  <title>Rock Paper - Explore Salons</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/custom.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="moment-with-locales.js"></script>
  <script src="bootstrap-datetimepicker.js"></script>
  <link href="bootstrap-datetimepicker.css" rel="stylesheet">
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
  </script>
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker5').datetimepicker();
            });
        </script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/logo.png" class = "img-responsive" style = "width:180px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">About</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Projects</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Register Barber Shop</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container text-center" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<div class = "row" style = "border-style:solid;border-width:4px;padding: 5px;margin: 5px;color:white;border-radius:15px;">
		<h4 style = "color:white;">&#10004;Select Location - &#10004;Choose Barbershop - &#10004;Choose services - <b><i>Select Timeslot</i></b> - Make Payment</h4>
	</div>
</div>
<br><br>
<div class = "container">
	<div class = "jumbotron col-xs-12" style = "background:rgba(0,0,0,0.7);">
		<div class = "container text-center" style = "width:300px;">
		<h3 style = "color:white;">Select a time slot</h3>
		<form method = "POST" action = "payment.php">
		<input type='text' name = "time_from" class="form-control" id='datetimepicker4'/><br>
		<input type = "submit" value = "Submit" class = "form-control">
		</div>
		</form>
</div>
</div>
<hr>
<div class = "container">
	<div class = "jumbotron col-xs-12" style = "background:rgba(255,255,255,0.7);">
		<div class = "container text-center">
			<h3>These slots are already booked</h3>
			<h5>Bookings are not allowed on an already booked slot</h5>
		</div>
		<br><br>
	</div>
</div>
<br><br><br><br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-4">
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.facebook.com/rockpaperindia"><img src = "images/facebook_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.instagram.com/rockpaperindia/"><img src = "images/instagram_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.linkedin.com/rockpaperindia"><img src = "images/linkedin_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://plus.google.com/rockpaperindia"><img src = "images/google_plus_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-4">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
	Copyright © 2017 Rock Paper - All Rights Reserved.
  </center>
  </div>
</footer>
</body>
</html>