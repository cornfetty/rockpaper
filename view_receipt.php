<html lang="en">
<head>
  <title>Barbero</title>
  <link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="qrcode.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>
<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require_once('config/config.php');
	require_once('phpqrcode/qrlib.php');
	require_once 'classes/phpmailer/mail.php';
	$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
	if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
	$sql = "SELECT * FROM booking,user WHERE booking.validation = '".$_GET["id"]."' AND booking.uid = user.uid";
	if ($result = mysqli_query($mysqli, $sql)) {
		while($row = mysqli_fetch_assoc($result)) {
	echo '<div class = "container"><div class = "row"><div class = "col-sm-6">';
	echo '<h2>Barbero Appointment Booking Receipt</h2>';
	echo '<hr><div id = "qrcode" style = "float:right;"></div>';
	echo '<h4>Customer Name: '.$row["firstname"].' '.$row["lastname"].'</h4>';
	echo '<h4>Customer Email: '.$row["email"].'';
	echo '<h4>Customer Mobile: '.$row["mobile"].'<hr>';
	echo '<h3>Order details</h3>';
	$sql = "SELECT * FROM barbershop WHERE barbershop.bsid = '".$row["bsid"]."'";
	if ($result = mysqli_query($mysqli, $sql)) {
	while($row3 = mysqli_fetch_assoc($result)) {
		echo '<h4>Shop: '.$row3["shopname"].'</h4><h5>Address: '.$row3["addressline1"].'</h5><h5>&nbsp;'.$row3["addressline2"].'</h5><h5> Contact: '.$row3["shopcontact"].'</h5>';
	}
	} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
	}
	
		$sql = "SELECT name as barbername FROM barberofshop WHERE bofsid = '".$row["bid"]."'";
		if ($result = mysqli_query($mysqli, $sql)) {
		while($row1 = mysqli_fetch_assoc($result)) {
		echo '<h4>Barber: '.$row1["barbername"].'</h4><h4>Services:';
		}
		}else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
		}
		$sql = "SELECT * FROM service,bookingservice WHERE bookingservice.bookid = '".$row["bookid"]."' AND service.sid = bookingservice.sid";
		if ($result = mysqli_query($mysqli, $sql)) {
		while($row2 = mysqli_fetch_assoc($result)) {
			echo '<h5>'.$row2["type"].'</h5><h5>Rs. '.$row2["price"].' | Duration '.$row2["duration"].' minutes</h5><hr>';			
		
		}
		}else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
		}
		echo '<h4><div style = "float:left;">Total price (Rs.): '.$row["totalamount"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Duration (minutes): '.$row["totaltime"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Start time: '.$row["booktime"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">End time: '.$row["finishtime"].'</div></h4><br><br><br><h5><i>A copy of the receipt has been sent to your registered email</i></h5>
<h5><i>Thank you for choosing barbero.in</i></h5><form>
    <input type="button" name="print" class = "form-control" value="Print" onClick="window.print()">
</form></div><div class = "col-sm-6"></div></div></div>';
		}
	} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
					}
?>

<script type="text/javascript">
var id = <?php echo $_GET["id"];?>;
var qrcode = new QRCode("qrcode", {
    text: "http://www.barbero.in/view_receipt.php?id="+id,
    width: 64,
    height: 64,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});
</script>
</body>
</html>